"""Evaluate a given policy by visualizing."""
import torch

import gym

from AS.architectures.dqn import DQN
from AS.policies import DQNBasedPolicy
from AS.architectures.ddqn import DuelingDQN

env = gym.make('LunarLander-v2')

dqn = DQN(env.observation_space, env.action_space)
dqn.load_state_dict(torch.load('wandb/run-20211220_223556-1oto8khk/files/checkpoint.pth'))
dqn_policy = DQNBasedPolicy(dqn)

obs = env.reset()
while True:
    action = dqn_policy(torch.from_numpy(obs))
    obs, reward, done, info = env.step(action)
    env.render()
    if done:
        obs = env.reset()
