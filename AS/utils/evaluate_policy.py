"""Evaluate policy."""
import gym
import torch
import AS.policy_types


def evaluate_policy(env: gym.Env, policy: AS.policy_types.Policy, n_samples: int) -> float:
    """

    :param env:
    :param policy:
    :param n_samples:
    :return:
    """
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    total_rewards = []
    for i in range(n_samples):
        obs = env.reset()
        done = False
        total_rewards.append(0.)
        while not done:
            action = policy(torch.from_numpy(obs).to(device))
            obs, reward, done, info = env.step(action)
            total_rewards[i] += reward
    return sum(total_rewards) / len(total_rewards)
