import torch

from AS.policy_types import ContinuousPolicy, ContinuousAction
from AS.architectures.ddpg import ActorCritic


class DDPGBasedPolicy(ContinuousPolicy):
    def __init__(self, actor_critic: ActorCritic):
        self.actor_critic = actor_critic

    def decide_action(self, observation: torch.Tensor) -> ContinuousAction:
        return self.actor_critic.actor_forward(observation)
