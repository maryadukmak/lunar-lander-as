"""The Deep Deterministic Policy Gradient class."""
from typing import List
from math import prod
import torch
import torch.nn as nn
import gym


class ActorCritic(nn.Module):
    """
    The neural network architecture for Deep Deterministic Policy Gradient.
    Containing an actor and a critic, two separate networks but using the same feature-network.
    """

    def __init__(self, observation_space: gym.spaces.Box, action_space: gym.spaces.Box, fc1_units: int = 64,
                 fc2_units: int = 64, fc3_units: int = 64):
        super(ActorCritic, self).__init__()

        self.feature_net = nn.Sequential(
            nn.Linear(prod(observation_space.shape), fc1_units),
            nn.ReLU(),
            nn.Linear(fc1_units, fc2_units),
            nn.ReLU(),
        )

        self.actor = nn.Sequential(
            nn.Linear(fc2_units, fc3_units),
            nn.ReLU(),
            nn.Linear(fc3_units, prod(action_space.shape))
        )

        self.critic = nn.Sequential(
            nn.Linear(fc2_units + prod(action_space.shape), fc3_units),
            nn.ReLU(),
            nn.Linear(fc3_units, 1)
        )

    def actor_forward(self, obs: torch.Tensor) -> torch.Tensor:
        """
        Forward propagate the given observation through the actor network to get an action.

        :param obs: The observation.
        :return: A tensor of representing an action in a continuous action-space.
        """
        return self.actor(self.feature_net(obs))

    def critic_forward(self, obs: torch.Tensor, action: torch.Tensor) -> torch.Tensor:
        """
        Forward propagate the given observation and action through the critic network to get an estimated q value.

        :param obs: The observation as context to judge.
        :param action: The action to judge.
        :return: The Q value of the given action on the given observation.
        """
        return self.critic(torch.cat([self.feature_net(obs), action], 1))

    def get_actor_parameters(self) -> List[nn.parameter.Parameter]:
        return list(self.feature_net.parameters()) + list(self.actor.parameters())

    def get_critic_parameters(self) -> List[nn.parameter.Parameter]:
        return list(self.feature_net.parameters()) + list(self.critic.parameters())
