"""The Dueling Deep Q Network class."""
from math import prod
import torch
import torch.nn as nn
import gym


class DuelingDQN(nn.Module):
    """The architecture of the Dueling Deep Q Network."""

    def __init__(self, observation_space: gym.spaces.Box, action_space: gym.spaces.Discrete, fc1_units: int = 64,
                 fc2_units: int = 64):
        super(DuelingDQN, self).__init__()

        self.feature_net = nn.Sequential(
            nn.Linear(prod(observation_space.shape), fc1_units),
            nn.ReLU(),
            nn.Linear(fc1_units, fc2_units),
            nn.ReLU(),
        )

        self.value_net = nn.Sequential(
            nn.Linear(fc2_units, fc2_units),
            nn.ReLU(),
            nn.Linear(fc2_units, 1),
        )

        self.advantage_net = nn.Sequential(
            nn.Linear(fc2_units, fc2_units),
            nn.ReLU(),
            nn.Linear(fc2_units, action_space.n),
        )

    def forward(self, obs: torch.Tensor) -> torch.Tensor:
        """
        Forward propagate the given observation through the neural network
        to get and return the estimated Q values for each action.

        :param obs: The observation.
        :return: A tensor of estimated Q values for each action.
        """
        features = self.feature_net(obs)
        value = self.value_net(features)
        advantage = self.advantage_net(features)
        q_value = value + (advantage - advantage.mean())

        return q_value
