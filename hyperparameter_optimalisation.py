"""Hyperparamter optimization sweep run. It does multiple runs and logs it to wandb."""

import gym
import wandb
from AS.architectures.dqn import DQN
from AS.architectures.ddqn import DuelingDQN
from AS.algorithms.train_dqn import train


config = {
    'env-name': 'LunarLander-v2',
    'n_episodes': 2000,
    'buffer_size': 100_000,
    'batch_size': 64,
    'discount_factor': 0.99,
    'epsilon_decay': 0.995,
    'learning_rate': 0.0005,
    'update_rate': 1,
    'tau': 0.001,
    'save_rate': 600,
    'architecture': "DQN",
}


with wandb.init(config=config):
    config = wandb.config
    env = gym.make('LunarLander-v2')
    architecture = {
        'DQN': DQN,
        'DuelingDQN': DuelingDQN
    }[config.architecture]
    train(
        env=env,
        dqn=architecture(env.observation_space, env.action_space, fc1_units=config.fc1_units, fc2_units=config.fc2_units),
        n_episodes=round(config.n_episodes),
        buffer_size=round(config.buffer_size),
        batch_size=round(config.batch_size),
        discount_factor=config.discount_factor,
        epsilon_decay=config.epsilon_decay,
        learning_rate=config.learning_rate,
        update_rate=config.update_rate,
        tau=config.tau,
        save_rate=600,
    )
