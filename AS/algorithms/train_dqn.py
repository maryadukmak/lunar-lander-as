"""The algorithm to train a Deep Q Network or a Dueling Deep Q Network."""
from typing import Union
from copy import deepcopy
import os

import gym
import torch
from torch import optim
import torch.nn.functional as f
import wandb
from alive_progress import alive_it

from AS.architectures.dqn import DQN
from AS.architectures.ddqn import DuelingDQN
from AS.replay_memory import MemoryBuffer, Transition, TransitionBatch
from AS.policies import DQNBasedPolicy, EpsilonGreedyPolicy
from AS.policy_types import Policy


def train(env: gym.Env, dqn: Union[DQN, DuelingDQN], n_episodes: int = 10000, buffer_size: int = 100000,
          batch_size: int = 64, discount_factor: float = 0.99, epsilon_decay: float = .995,
          learning_rate: float = 0.00015, update_rate: int = 4, tau: float = 0.001, save_rate: int = 10) -> Policy:
    """
    Train a policy using deep Q learning.

    The accepted network architectures are DQN and DuelingDQN.
    This naturally only works on environments with continuous observation space and discrete action space.

    :param env: Gym environment with continuous observation space and discrete action space.
    :param dqn: Pytorch neural network, currently supporting DQN and DuelingDQN.
    :param n_episodes: Number of episodes to train, the model is frequently saved so stopping early is no problem.
    :param buffer_size: Size of the replay memory.
    :param batch_size: Number of samples to run parallel in a batch.
    :param discount_factor: Discount factor (aka γ / gamma) for the bellman equation.
    :param epsilon_decay: Rate at which the epsilon decays, should be close to 1.
    :param learning_rate: Learning rate for the gradient descent, should be small.
    :param update_rate: In how many steps the target network should be updated
    :param tau: Tau of soft update.
    :param save_rate: Rate at which to save the model, higher rate means saving less often.
    :return: The trained policy.
    """
    assert isinstance(env.action_space, gym.spaces.Discrete)
    assert isinstance(env.observation_space, gym.spaces.Box)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    q_primary = deepcopy(dqn).to(device)
    q_target = deepcopy(dqn).to(device)
    optimizer = optim.Adam(q_primary.parameters(), lr=learning_rate)
    policy = DQNBasedPolicy(q_primary)
    epsilon_policy = EpsilonGreedyPolicy(policy, env.action_space)
    replay_memory = MemoryBuffer(buffer_size)
    total_steps = 0

    for episode in alive_it(range(n_episodes)):
        obs = env.reset()
        done = False
        total_reward = 0.
        while not done:
            # Explore the environment one step
            action = epsilon_policy(torch.from_numpy(obs).to(device))
            next_obs, reward, done, info = env.step(action)
            replay_memory.add(Transition(obs, action, reward, done, next_obs))
            total_steps += 1
            obs = next_obs
            total_reward += reward

            if (total_steps + 1) % update_rate == 0 and len(replay_memory) >= batch_size:
                # Learn
                batch = replay_memory.sample(batch_size, device)
                # Calculate loss
                loss = compute_loss(q_primary, q_target, batch, discount_factor)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                # Update target network
                soft_update(q_primary, q_target, tau)

                wandb.log({'loss': loss}, step=total_steps)
            if save_rate > 0:
                if total_steps % save_rate == 0:
                    torch.save(q_target.state_dict(), os.path.join(wandb.run.dir, "checkpoint.pth"))
        wandb.log({'total-reward': total_reward, 'epsilon': epsilon_policy.epsilon}, step=total_steps)

        # Decay epsilon
        epsilon_policy.decay_eps(decay_rate=epsilon_decay)

    return policy


def compute_loss(q_primary_net: DQN, q_target_net: DQN, batch: TransitionBatch,
                 discount_factor: float = 0.99) -> torch.Tensor:
    """
    Compute the loss of the predicted Q values on the given batch.

    :param q_primary_net: Primary deep Q network.
    :param q_target_net: Target deep Q network.
    :param batch: Batch of observations, actions, rewards, dones and next-observations.
    :param discount_factor: Discount factor (aka γ / gamma) for in the bellman equation.
    :return: Loss with grad_fn.
    """
    observations, actions, rewards, dones, next_obs = batch

    # Calculate target value using bellman equation
    with torch.no_grad():
        next_q = q_target_net(next_obs).gather(1, q_primary_net(next_obs).argmax(1).unsqueeze(1)).squeeze()
    q_star = rewards + torch.logical_not(dones) * discount_factor * next_q

    q_pred = q_primary_net(observations).gather(1, actions.unsqueeze(1)).squeeze()
    # Loss calculation (we used Mean squared error)
    loss = f.mse_loss(q_pred.float(), q_star.float())
    return loss


def soft_update(q_primary: DQN, q_target: DQN, tau: float = 0.001) -> None:
    """
    Inline update target network using the primary network and tau.

    :param q_primary: Primary deep Q network.
    :param q_target: Target deep Q network.
    :param tau: Tau defines the "softness" of the update.
    """
    for target_param, primary_param in zip(q_target.parameters(), q_primary.parameters()):
        target_param.data.copy_(tau * primary_param.data + (1.0 - tau) * target_param.data)
