"""The deep Q learning class."""
import gym
import torch
import torch.nn as nn
from math import prod


class DQN(nn.Module):
    """The architecture of the Deep Q Network."""

    def __init__(self, observation_space: gym.spaces.Box, action_space: gym.spaces.Discrete,
                 fc1_units: int = 64, fc2_units: int = 64) -> None:
        super(DQN, self).__init__()

        self.features_net = nn.Sequential(
            nn.Linear(prod(observation_space.shape), fc1_units),
            nn.ReLU(),
            nn.Linear(fc1_units, fc2_units),
            nn.ReLU(),
            nn.Linear(fc2_units, action_space.n)
        )

    def forward(self, obs: torch.Tensor) -> torch.Tensor:
        """
        Forward propagate the given observation through the neural network
        to get and return the estimated Q values for each action.

        :param obs: The observation.
        :return: A tensor of estimated Q values for each action.
        """
        return self.features_net(obs)
