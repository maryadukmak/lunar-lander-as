"""Starts training a policy with the given configuration, a Weights and Biases run will be created as well."""
import gym
import wandb

from AS.algorithms.train_dqn import train
from AS.architectures.dqn import DQN
from AS.architectures.ddqn import DuelingDQN

# Configuration
config = {
    'env-name': 'LunarLander-v2',
    'n_episodes': 2000,
    'buffer_size': 100_000,
    'batch_size': 64,
    'discount_factor': 0.99,
    'epsilon_decay': 0.995,
    'learning_rate': 0.0005,
    'update_rate': 1,
    'tau': 0.001,
    'save_rate': 600,
    'architecture': 'DQN',
}

# Initialisation
wandb.init(project="lunar-lander-project", entity='as-team0')
env = gym.make(config['env-name'])
architecture = {
    'DQN': DQN,
    'DuelingDQN': DuelingDQN
}[config['architecture']]

# Training
dqn_policy = train(
    env=env,
    dqn=architecture(env.observation_space, env.action_space),
    n_episodes=config['n_episodes'],
    buffer_size=config['buffer_size'],
    batch_size=config['batch_size'],
    discount_factor=config['discount_factor'],
    epsilon_decay=config['epsilon_decay'],
    learning_rate=config['learning_rate'],
    update_rate=config['update_rate'],
    tau=config['tau'],
    save_rate=config['save_rate'],
)
wandb.finish()
