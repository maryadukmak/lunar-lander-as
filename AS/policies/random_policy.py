"""Random policy chooses a random action based on the action-space of the environment."""
import gym

from AS.policy_types import Policy, Action, Observation


class RandomPolicy(Policy):
    """Random policy that choose action randomly."""

    def __init__(self, action_space: gym.spaces.Space) -> None:
        self.action_space = action_space

    def decide_action(self, observation: Observation) -> Action:
        """
        Get action randomly using the environment's action space.

        :param observation: Observation, as abstract as it is.
        :return A random action.
        """
        return self.action_space.sample()
