import setuptools

install_requires = [
    'numpy',
    'torch',
    'gym',
    'wandb',
    'alive-progress'
]


extras = {
    "dev": [
        'flake8',
        'flake8-blind-except',
        "flake8-builtins",
        "flake8-docstrings",
        "flake8-logging-format"]
}

setuptools.setup(
    name='Lunar-Lander-AS',
    version='0.0.1',
    packages=setuptools.find_packages(),
    install_requires=install_requires,
    extras_require=extras,
    python_requires='>=3.7',
    author='Maria and Lucas',
    author_email='Maria.dukmak@student.hu.nl',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    include_package_data=True,
)
