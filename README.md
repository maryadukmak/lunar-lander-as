# LunarLander with Deep Q learning 
[![Python 3.8](https://img.shields.io/badge/python-3.8-blue.svg)](https://www.python.org/downloads/release/python-380/)

In deze repo is er implementatie van de Lunar Lander Gym environment te vinden met de volgende algoritmes:
- Dubbel Deep Q learning (DQN)
- Dueling Deep Q learning (DDQN)
- Deep Deterministic Policy Gradient (DDPG)

## Installation 
Voor het installeren van de dependencies:
```bash 
pip install . 
```
Als je een developer bent, run de volgende command ook:
```bash
pip install .[dev]
```

## Code structuur 
De code van dit project is verdeeld in de volgende:

- Netwerken architecturen 
- Policies 
- Algoritmes

## Resultaten 
Voor het bekijken van de resultaten is er een [rapport](https://wandb.ai/as-team0/lunar-lander-project/reports/Resultaten-Analyse---VmlldzoxMzY0NDUw?accessToken=fz97f1gca469n35j9tppqjb4rjf0omi9xq1336nf678c5haajs43g31ior94xx98) beschikbaar op weight and biases.
Hieronder is er een korte demo van de resultaten per algoritme te vinden.

### Dubbel Deep Q learning (DQN)

![DQN getrainde agent](./readme_assest/dqn_300.gif)
### Dueling Deep Q learning (DDQN)

![DDQN getrainde agent](./readme_assest/ddqn_fc1_128.gif)

### Deep Deterministic Policy Gradient (DDPG)

## Hyper-parameter optimalisation
Opzoek gaan naar de beste hyperparamters voor het model is lastig. 
Daarom hebben we hier gebruik gemaakt van hyperparamter sweep functionalteit. Om meer hierover te lezen en zien zie [dit](https://wandb.ai/as-team0/hyperparamenters-opt/reports/Hyperparameters-optimalisatie-sweep---VmlldzoxMzY2MTU5?accessToken=6jrzxa6v8ihq5zmmsfnew34bfw9qah8uc68d3fq98r6w3zd49f8x9dntdwh6q248) rapport.
Als je een developer bent en een sweep willen runnen, run het volgende:
```bash
- wandb sweep hyperparameter-optimalisation.yaml 
- wndb agent SWEEP_ID # SWEEP_ID wordt teruggegeven bij de vorige command
```

## License 
MIT License