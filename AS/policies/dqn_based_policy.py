"""Deep q Network based policy. Use as a wrap on the policies."""
import torch

from AS.architectures.dqn import DQN
from AS.policy_types import DiscretePolicy, DiscreteAction, Observation


class DQNBasedPolicy(DiscretePolicy):
    """Deep q Network based policy."""

    def __init__(self, dqn: DQN) -> None:
        self.dqn = dqn

    def decide_action(self, observation: Observation) -> DiscreteAction:
        """
        Get an action using a dqn network.
        :param observation:
        :return: the best action based on the network values.
        """
        with torch.no_grad():
            q_values = self.dqn(observation)
        return torch.argmax(q_values).item()
