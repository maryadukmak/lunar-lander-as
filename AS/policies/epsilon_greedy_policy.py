"""Epsilon greedy policy."""
import gym
import random
import numpy as np
from AS.policy_types import DiscretePolicy, DiscreteAction, Observation


class EpsilonGreedyPolicy(DiscretePolicy):
    """Epsilon greedy policy based on a discrete policy."""

    def __init__(self, policy: DiscretePolicy, action_space: gym.spaces.Discrete, epsilon: float = 1.) -> None:
        self.policy = policy
        self.epsilon = epsilon
        self.action_space = action_space

    def decide_action(self, observation: Observation) -> DiscreteAction:
        """
        Get action based on possible action and
        probability of taking that action.
        :param observation
        :return greedy based chosen action
        """
        possible_actions = np.arange(self.action_space.n)
        optimal_action = self.policy(observation)
        probabilities = [
            1 - self.epsilon + self.epsilon / len(possible_actions)
            if a == optimal_action
            else self.epsilon / len(possible_actions)
            for a in possible_actions
        ]
        action, = random.choices(possible_actions, probabilities)
        return action

    def decay_eps(self, min_epsilon: float = .1, decay_rate: float = .995) -> None:
        """
        Decrease epsilon using decay rate.
        :param min_epsilon: the minimum epsilon
        :param decay_rate: the rate to decrease epsilon
        """
        self.epsilon = max(min_epsilon, self.epsilon * decay_rate)
