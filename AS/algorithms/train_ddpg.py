from copy import deepcopy
import os
import torch
import torch.nn.functional as f
from torch import optim
import gym
from alive_progress import alive_it
import wandb

from AS.replay_memory import MemoryBuffer, Transition, TransitionBatch
from AS.architectures.ddpg import ActorCritic
from AS.policies.ddpg_based_policy import DDPGBasedPolicy


def train_ddpg(env: gym.Env, actor_critic: ActorCritic, buffer_size: int, n_episodes: int,
               epsilon_decay: float, update_rate: int, n_updates: int, batch_size: int, discount_factor: float,
               learning_rate: float, tau: float, save_rate: int) -> DDPGBasedPolicy:

    assert isinstance(env.observation_space, gym.spaces.Box)
    assert isinstance(env.action_space, gym.spaces.Box)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    actor_critic = deepcopy(actor_critic).to(device)
    target_ac = deepcopy(actor_critic).to(device)
    replay_buffer = MemoryBuffer(buffer_size)
    actor_critic_policy = DDPGBasedPolicy(actor_critic)
    actor_optimizer = optim.Adam(actor_critic.get_actor_parameters(), lr=learning_rate)
    critic_optimizer = optim.Adam(actor_critic.get_critic_parameters(), lr=learning_rate)
    epsilon = 1.

    total_steps = 0

    for episode in alive_it(range(n_episodes)):
        obs = env.reset()
        done = False
        total_reward = 0.
        while not done:
            with torch.no_grad():
                action = torch.clip(actor_critic_policy(torch.from_numpy(obs).to(device)) + epsilon, torch.from_numpy(env.action_space.low).to(device), torch.from_numpy(env.action_space.high).to(device)).cpu().numpy()
            next_obs, reward, done, info = env.step(action)
            total_reward += reward
            replay_buffer.add(Transition(obs, action, reward, done, next_obs))
            total_steps += 1

            if (total_steps + 1) % update_rate == 0 and len(replay_buffer) >= batch_size:
                for _ in range(n_updates):
                    batch = replay_buffer.sample(batch_size, device)

                    critic_loss = compute_critic_loss(actor_critic, target_ac, batch, discount_factor)
                    critic_optimizer.zero_grad()
                    critic_loss.backward()
                    critic_optimizer.step()

                    actor_loss = compute_actor_loss(actor_critic, batch)
                    actor_optimizer.zero_grad()
                    actor_loss.backward()
                    actor_optimizer.step()

                    soft_update(actor_critic, target_ac, tau)
            if save_rate > 0:
                if total_steps % save_rate == 0:
                    torch.save(actor_critic.state_dict(), os.path.join(wandb.run.dir, "checkpoint.pth"))
        epsilon = max(epsilon * epsilon_decay, 0.1)
        wandb.log({'total-reward': total_reward, 'epsilon': epsilon}, step=total_steps)

    return actor_critic_policy
                    

def compute_critic_loss(actor_critic: ActorCritic, target_ac: ActorCritic, 
                        batch: TransitionBatch, discount_factor: float = 0.99) -> torch.Tensor:
    observations, actions, rewards, dones, next_obs = batch
    with torch.no_grad():
        action = target_ac.actor_forward(next_obs)
        target = rewards + discount_factor * torch.logical_not(dones) * target_ac.critic_forward(next_obs, action).squeeze()
    pred = actor_critic.critic_forward(observations, actions).squeeze()
    loss = f.mse_loss(pred.float(), target.float())
    return loss


def compute_actor_loss(actor_critic: ActorCritic, batch: TransitionBatch) -> torch.Tensor:
    observations, actions, rewards, dones, next_obs = batch
    loss = torch.mean(actor_critic.critic_forward(observations, actor_critic.actor_forward(observations)))
    return loss


def soft_update(actor_critic: ActorCritic, target_ac: ActorCritic, tau: float = 0.001) -> None:
    """
    Inline update target network using the primary network and tau.

    :param actor_critic: Primary actor critic network.
    :param target_ac: Target actor critic network.
    :param tau: Tau defines the "softness" of the update.
    """
    for target_param, primary_param in zip(target_ac.parameters(), actor_critic.parameters()):
        target_param.data.copy_(tau * primary_param.data + (1.0 - tau) * target_param.data)
