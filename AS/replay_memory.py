"""Memory buffer class and type classes."""
import random
import torch
import numpy as np
from collections import deque
from typing import Tuple, NamedTuple

from AS.policy_types import Observation, Action


class Transition(NamedTuple):
    """
    Transition is just a collection of previous_observation, action, reward, done and next observation.
    This represents a transition from one state to the next state in an environment.
    """

    previous_observation: Observation
    action: Action
    reward: float
    done: int
    next_observation: Observation


TransitionBatch = Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]


class MemoryBuffer:
    """Memory class to store transitions."""

    def __init__(self, buffer_size: int) -> None:
        self.memory = deque(maxlen=buffer_size)

    def add(self, transition: Transition) -> None:
        """
        Add a new experience to the memory.

        :param: transition: Transition from one state to the next.
        """
        self.memory.append(transition)

    def sample(self, batch_size: int = 64, device: torch.device = 'cpu') -> TransitionBatch:
        """
        Get a random batch sample from the memory.

        :param batch_size: Number of transitions to return, called a batch of transitions.
        :param device: CPU or GPU.
        :return: Batch of transitions.
        """
        experiences = random.sample(self.memory, k=batch_size)
        observations, actions, rewards, dones, next_observations = map(lambda x: torch.from_numpy(np.array(x)).to(device),
                                                                       zip(*experiences))
        return observations, actions, rewards, dones, next_observations

    def __len__(self) -> int:
        """Get number of transitions in memory."""
        return len(self.memory)
