"""Base policy types."""
from typing import Union
import numpy as np
import torch
from abc import ABCMeta, abstractmethod

DiscreteAction = int
ContinuousAction = torch.Tensor
Action = Union[DiscreteAction, ContinuousAction]
Observation = np.ndarray


class Policy(metaclass=ABCMeta):
    """Base of every policy in this project."""

    @abstractmethod
    def decide_action(self, observation: Observation) -> Action:
        """
        Decide an action based on the given observation using this policy.

        :param observation: Observation, as abstract as that is.
        :return: Chosen action.
        """
        pass

    def __call__(self, *args, **kwargs) -> Action:
        """Just call decide_action but fancy."""
        return self.decide_action(*args, **kwargs)


class DiscretePolicy(Policy, metaclass=ABCMeta):
    """Specifically a policy that gives discrete actions."""

    @abstractmethod
    def decide_action(self, observation: Observation) -> DiscreteAction:
        """
        Decide an action based on the given observation using this policy.

        :param observation: Observation, as abstract as that is.
        :return: Chosen action.
        """
        pass

    def __call__(self, *args, **kwargs) -> DiscreteAction:
        """Just call decide_action but fancy."""
        return self.decide_action(*args, **kwargs)


class ContinuousPolicy(Policy, metaclass=ABCMeta):
    """"Specifically a policy that gives continuous actions."""

    @abstractmethod
    def decide_action(self, observation: Observation) -> ContinuousAction:
        """
        Decide an action based on the given observation using this policy.

        :param observation: Observation, as abstract as that is.
        :return: Chosen action.
        """
        pass

    def __call__(self, *args, **kwargs) -> ContinuousAction:
        """Just call decide_action but fancy."""
        return self.decide_action(*args, **kwargs)
